<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Imgup extends Controller {

	public function action_index()
	{
		$this->response->body(View::factory('index'));
	}

	public function action_upload()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		if ($this->request->method() == Request::POST)
		{
			try
			{
				$image = $_FILES['image'];
				$filename = '';

				// If not a valid file or file type
				if (Upload::not_empty($image) &&
					Upload::valid($image) &&
					Upload::type($image, array('jpg', 'jpeg', 'png')))
				{
					$path = DOCROOT . 'uploads/';

					// Upload the file and save it's name in $filename
					$filename = Upload::save($image, NULL, $path);

					// If upload failed
					if (!$filename)
					{
						throw new Exception('Unable to upload image');
					}
					$filename = basename($filename);
				}
				else
				{
					throw new Exception('Invalid file');
				}

				$new_image = new Model_Image();
				
				$imageData = array(
					'title' => $this->request->post('title'),
					'path' => 'uploads/' . $filename,
					'created_at' => date('Y-m-d H:i:s'),
				);
				$new_image->values($imageData);

				// Save details to db
				$new_image->save();
				
				$imageDetails = array(
					'id' => $new_image->id,
					'title' => $new_image->title,
					'path' => $new_image->path,
					'created_at' => $new_image->created_at,
				);

				return $this->response->body(
					json_encode(
						array(
							'success' => TRUE,
							'message' => 'Upload success',
							'data' => $imageDetails
						)
					)
				);
			}
			catch (Exception $e)
			{
				return $this->response->body(
					json_encode(
						array(
							'success' => FALSE,
							'message' => $e->getMessage(),
							'data' => NULL
						)
					)
				);
			}
		}
		else {
			return $this->response->body(
				json_encode(
					array(
						'success' => FALSE,
						'message' => 'Invalid request',
						'data' => NULL
					)
				)
			);
		}
	}

	public function action_display()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		try {
			$ormImage = ORM::factory('Image');
			$images = $ormImage->find_all();
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>false,
						'message' => $e->getMessage(),
						'data' => null
					)
				)
			);
		}

		$allImages = [];
		foreach ($images as $image) {
			$allImages[] = [
				'id'	=>	$image->id,
				'title' =>	$image->title,
				'path'	=>	$image->path,
				'filename' => basename($image->path),
				'created_at' => $image->created_at
			];
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>true,
					'message' => '',
					'data' => array(
						'total' => count($allImages),
						'records' => $allImages
					)
				)
			)
		);
	}

	public function action_edit()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$id = $this->request->post('id');

		try {
			$ormImage = ORM::factory('Image');
			$image = $ormImage->where('id', '=', $id)->find();

			if (! $image) {
				throw new Exception("Image not found");
			}
		} catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>FALSE,
						'message' => $e->getMessage(),
						'data' => NULL
					)
				)
			);
		}
		
		$filename = '';
		if (!empty($_FILES['image']['tmp_name']))		
		{
			$new_image = $_FILES['image'];
			try {
				// If not a valid file or file type
				if (Upload::not_empty($new_image) &&
					Upload::valid($new_image) &&
					Upload::type($new_image, array('jpg', 'jpeg', 'png')))
				{
					$path = DOCROOT . 'uploads/';

					// Upload the file and save it's name in $filename
					$filename = Upload::save($new_image, NULL, $path);

					// If upload failed
					if (!$filename)
					{
						throw new Exception('Unable to upload image');
					}
					$filename = basename($filename);
				}
			} catch (Exception $e) {
				return $this->response->body(
					json_encode(
						array(
							'success'=>FALSE,
							'message' => $e->getMessage(),
							'data' => NULL
						)
					)
				);
			}
		}

		$fillData = [];
		if (trim($this->request->post('title')) != '') {
			$fillData['title'] = trim($this->request->post('title'));
		}

		if ($filename != '') {
			$fillData['path'] = "uploads/" . $filename;
			$filename = $image->path;
		}

		$image->values($fillData);

		try {
			$image->save();

			// Delete the old image
			if ($filename != '') {
				unlink(DOCROOT . $filename);
			}
		}
		catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>FALSE,
						'message' => $e->getMessage(),
						'data' => NULL
					)
				)
			);
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>TRUE,
					'message' => '',
					'data' => $image
				)
			)
		);
	}

	public function action_delete()
	{
		$this->response->headers('Content-Type', 'application/json; charset=utf-8');
		$id = $this->request->post('id');

		try {
			$ormImage = ORM::factory('Image');
			$image = $ormImage->where('id', '=', $id)->find();

			if (! $image) {
				throw new Exception("Image not found");
			}
		}
		catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>FALSE,
						'message' => $e->getMessage(),
						'data' => NULL
					)
				)
			);
		}

		try {
			unlink(DOCROOT. $image->path);
			$image->delete();
		}
		catch (Exception $e) {
			return $this->response->body(
				json_encode(
					array(
						'success'=>FALSE,
						'message' => $e->getMessage(),
						'data' => NULL
					)
				)
			);
		}

		return $this->response->body(
			json_encode(
				array(
					'success'=>TRUE,
					'message' => 'Image Deleted',
					'data' => NULL
				)
			)
		);
	}
}