<?php
class Model_Image extends ORM {

	protected $_table_columns = array(
		'id' => array(),
		'title' => array(),
		'path' => array(),
		'created_at' => array(),
		'updated_at' => array()
	);
}