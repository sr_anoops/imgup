<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Imgup - Kohana Image Uploader</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="/assets/style.css" />
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">

</head>
<body>
	<h1>Image Uploader</h1>
	<button id="img_upload_launcher" class="upload_button">Upload</button>
	<div id="img_upload">
		<div id="img_upload_content">
			<form name="img_upload_form" id="img_upload_form" enctype="multipart/form-data">
				<input type="text" name="title" id="title" placeholder="Title">
				<input type="file" name="image" id="image">
			</form>
			<button class="upload_button" id="btnUpload">Upload</button>
			<button id="img_upload_close">Cancel</button>
			<p id="img_upload_message"></p>			
		</div>
	</div>

	<div id="img_edit">
		<div id="img_edit_content">
			<form name="img_edit_form" id="img_edit_form" enctype="multipart/form-data">
				<input type="hidden" name="id" id="edit_id">
				<input type="text" name="title" id="edit_title" placeholder="Title">
				<input type="file" name="image" id="edit_image">
			</form>
			<button class="edit_button" id="btnEdit">Update</button>
			<button id="img_edit_close">Cancel</button>
			<p id="img_edit_message"></p>
		</div>
	</div>

	<br><br>

	<table id="tbl_images">
		<thead>
			<tr>
				<th>Title</th>
				<th>Thumbnail</th>
				<th>Filename</th>
				<th>Date Added</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody id="contents">
		</tbody>
	</table>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="/assets/main.js"></script>
</body>
</html>