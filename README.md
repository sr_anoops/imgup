# Kohana Test Project - Image Uploader


## How to install

- Create a new virtual host on your server.
- Clone this repository to the webroot of this new virtual host.	
- Create a new mysql database named `imgup`
- Import the DB schema located at `db/imgup.sql` into this new database.
- Open the file `modules/database/config/database.php`. Modify the connection string, username and password with your own values.
- In case the pages are not rendering as expected, rename the file `example.htaccess` to `.htaccess`. Make sure your server is configured to allow url rewrites so that the .htaccess file works.

NOTE: The app is designed to work from the document root. If you put it inside a subfolder it might not work.

## Access the application

- The application can be accessed from your browser by accessing the following url: `http://localhost/imgup`. You may need to modify the hostname based on your virtual host settings.

## Usage

- You can upload a new file by clicking on the Green `Upload` button on the home page. Provide the `Title` and `Image` to be uploaded to the opened modal.

- The new uploaded file will be displayed in the table without refreshing the page.

- You can `Edit` or `Delete` the image by clicking the corresponding buttons in the table.