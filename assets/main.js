$(document).ready(function () {
	console.log('jQuery ready');
	$('#btnUpload').click(function () {
		var data = new FormData(document.forms.img_upload_form);
		$.ajax({
			url: '/imgup/upload',
			type: 'POST',
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			success: function (result) {
				console.log(result);
				$("#img_upload_content,#img_upload").toggleClass("active");
				displayImages();
			},
			error: function (result) {
				console.log('error');
				console.log(result);
			}
		});		
	});

	$('#btnEdit').click(function() {
		var formData = new FormData(document.forms.img_edit_form);

		$.ajax({
			url: '/imgup/edit',
			type: 'POST',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function (result) {
				console.log(result);
				$("#img_edit_content,#img_edit").toggleClass("active");
				displayImages();
			},
			error: function (result) {
				console.log('error');
				console.log(result);
			}
		});
	});

	displayImages();
});

function displayImages() {
	$.ajax({
		type: 'POST',
		url: '/imgup/display',
		cache: false,
		success: function (response) {
			console.log("success");
			console.log(response);			

			$('#contents').html('');

			if (response.success) {
				response.data.records.forEach(function (item, index) {

					$tdTitle = $('<td>').html(item.title);
					$tdThumbnail = $('<td>').html('<img class="thumb" style="height: 32px" src="' + item.path + '"/>');
					$tdFilename = $('<td>').html(item.filename);
					$tdCreatedOn = $('<td>').html(item.created_at);
					$editButton = $('<button class="edit_image edit_button" id="edit_' + item.id + '">').html('Edit').click(function () { editImage(item) });
					$deleteButton = $('<button class="delete_image delete_button" id="delete_' + item.id + '">').html('Delete').click(function () { deleteImage(item) });
					$tdActions = $('<td>').append($editButton).append($deleteButton);

					$('#contents').append(
						$('<tr>')
							.append($tdTitle)
							.append($tdThumbnail)
							.append($tdFilename)
							.append($tdCreatedOn)
							.append($tdActions)
					);
				});
			}
		},
		error: function (data) {
			console.log("error");
			console.log(data);
		}
	});
}

function deleteImage(item)
{
	var image_id = item.id;
	$.ajax({
		url: '/imgup/delete',
		type: 'POST',
		data: {
			id: image_id
		},
		success: function (result) {
			console.log(result);
			displayImages();
		},
		error: function(result) {
			console.log('error');
			console.log(result);			
		}
	});
}

function editImage(item)
{
	$('#img_edit_form').find("input[type=text], input[type=file], input[type=hidden]").val("");
	$('#img_edit_message').html('');
	$("#img_edit_content,#img_edit").toggleClass("active");
	$('#edit_title').val(item.title);
	$('#edit_id').val(item.id);
}

$('#img_edit_close').click(function(){
	$("#img_edit_content,#img_edit").toggleClass("active");
})

$(function () {
	$("#img_upload_launcher, #img_upload_close").click(function () {
		$('#img_upload_form').find("input[type=text], input[type=file]").val("");
		$('#img_upload_message').html('');
		$("#img_upload_content,#img_upload").toggleClass("active");
	});
});